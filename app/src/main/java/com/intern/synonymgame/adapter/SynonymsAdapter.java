package com.intern.synonymgame.adapter;

import android.app.Activity;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.intern.synonymgame.util.Constants;
import com.intern.synonymgame.util.Preferences;
import com.intern.synonymgame.R;
import com.intern.synonymgame.model.Synonym;

import java.util.ArrayList;
import java.util.Locale;

import pl.droidsonroids.gif.GifImageView;

public class SynonymsAdapter extends RecyclerView.Adapter<SynonymsAdapter.ViewHolder> {

    ArrayList<Synonym> synonyms;
    Activity activity;
    RecyclerView recyclerView;
    TextToSpeech textToSpeech;
    public float easyAccuracy = 0;
    public float mediumAccuracy = 0;
    public float hardAccuracy = 0;
    public float geniusAccuracy = 0;

    public SynonymsAdapter(ArrayList<Synonym> synonyms, final Activity activity) {
        this.synonyms = synonyms;
        this.activity = activity;

        textToSpeech = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS){
                    int result = textToSpeech.setLanguage(Locale.ENGLISH);
                    if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                        Toast.makeText(activity, "Error!!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.synonym_word,parent,false);
        recyclerView = (RecyclerView) parent;
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Synonym synonym = synonyms.get(position);
        holder.synonym_word_tv.setVisibility(View.VISIBLE);
        holder.synonym_word_tv.setText(synonym.getKey1());
        holder.synonym_word_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView selected_word = activity.findViewById(R.id.selected_word);
                selected_word.setVisibility(View.VISIBLE);
                selected_word.setText(holder.synonym_word_tv.getText());
                GifImageView word_status = activity.findViewById(R.id.word_status);
                word_status.setVisibility(View.GONE);
                textToSpeech.speak(String.valueOf(holder.synonym_word_tv.getText()),TextToSpeech.QUEUE_FLUSH,null);
                if(Preferences.hasPreference(Constants.FIRST_ENTRY)){
                    TextView firstItemTextView = (TextView) recyclerView.findViewHolderForAdapterPosition(Preferences.getInt(Constants.FIRST_ENTRY_ID, -1)).itemView.findViewById(R.id.synonym_word_tv);
                    String secondEntry = holder.synonym_word_tv.getText().toString();
                    if(Preferences.getString(Constants.FIRST_ENTRY, "NA").contentEquals(secondEntry)) {
                        if (holder.getAdapterPosition() != Preferences.getInt(Constants.FIRST_ENTRY_ID, -1)) {
                            try {
                                word_status.setVisibility(View.VISIBLE);
                                word_status.setImageResource(R.drawable.correct);
                                holder.synonym_word_tv.setVisibility(View.GONE);
                                firstItemTextView.setVisibility(View.GONE);
                                boolean isAnyViewVisible = false;

                                for (int k=0; k< synonyms.size(); k++ ){
                                    TextView tv = (TextView) recyclerView.findViewHolderForAdapterPosition(k).itemView.findViewById(R.id.synonym_word_tv);
                                    if(tv.getVisibility() ==  View.VISIBLE){
                                        isAnyViewVisible = true;
                                        break;
                                    }
                                }

                                if(!isAnyViewVisible) {
                                    RelativeLayout gameLayout = activity.findViewById(R.id.gameLayout);
                                    RelativeLayout summaryLayout = activity.findViewById(R.id.summaryLayout);
                                    TextView countdown = activity.findViewById(R.id.countdown);
                                    TextView level = activity.findViewById(R.id.level);
                                    String timeLeft = (String) countdown.getText();
                                    String getLevel = (String) level.getText();
                                    if (getLevel.equalsIgnoreCase("easy")){
                                        easyAccuracy = Float.parseFloat(timeLeft) / 30 * 100;
                                    }else if(getLevel.equalsIgnoreCase("medium")){
                                        mediumAccuracy = Float.parseFloat(timeLeft) / 50 * 100;
                                    }else if(getLevel.equalsIgnoreCase("hard")){
                                        hardAccuracy = Float.parseFloat(timeLeft) / 70 * 100;
                                    }else if(getLevel.equalsIgnoreCase("genius")){
                                        geniusAccuracy = Float.parseFloat(timeLeft) / 80 * 100;
                                    }
                                    //Toast.makeText(activity, easyAccuracy + " Seconds", Toast.LENGTH_SHORT).show();
                                    gameLayout.setVisibility(View.GONE);
                                    summaryLayout.setVisibility(View.VISIBLE);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        firstItemTextView.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
                        firstItemTextView.setTextColor(activity.getResources().getColor(R.color.colorBlack));
                        Preferences.removePreference(Constants.FIRST_ENTRY);
                        Preferences.removePreference(Constants.FIRST_ENTRY_ID);
                    } else{
                        word_status.setVisibility(View.VISIBLE);
                        word_status.setImageResource(R.drawable.incorrect);
                        firstItemTextView.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
                        firstItemTextView.setTextColor(activity.getResources().getColor(R.color.colorBlack));
                        Preferences.removePreference(Constants.FIRST_ENTRY);
                        Preferences.removePreference(Constants.FIRST_ENTRY_ID);
                    }
                } else {
                    holder.synonym_word_tv.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    holder.synonym_word_tv.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                    Preferences.putInt(Constants.FIRST_ENTRY_ID, holder.getAdapterPosition());
                    Preferences.putString(Constants.FIRST_ENTRY, synonym.getKey2());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return synonyms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView synonym_word_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            synonym_word_tv = itemView.findViewById(R.id.synonym_word_tv);
        }
    }
}
