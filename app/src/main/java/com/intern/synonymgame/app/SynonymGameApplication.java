package com.intern.synonymgame.app;

import android.app.Application;

import com.intern.synonymgame.util.Preferences;

public class SynonymGameApplication extends Application {
    private static SynonymGameApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Preferences.initPrefs(this);

    }

    public static SynonymGameApplication getInstance() {
        return instance;
    }


}