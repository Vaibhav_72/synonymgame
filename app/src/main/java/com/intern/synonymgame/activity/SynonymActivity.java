package com.intern.synonymgame.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.intern.synonymgame.util.Constants;
import com.intern.synonymgame.model.Crossword;
import com.intern.synonymgame.util.Preferences;
import com.intern.synonymgame.R;
import com.intern.synonymgame.model.Synonym;
import com.intern.synonymgame.adapter.SynonymsAdapter;
import com.intern.synonymgame.adapter.SynonymsSummaryAdapter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import pl.droidsonroids.gif.GifImageView;


public class SynonymActivity extends AppCompatActivity {

    SwipeRefreshLayout srl;
    RelativeLayout gameLayout;
    RelativeLayout game_options_menu;
    TextView question;
    ImageButton rules;
    View divider;
    RecyclerView synonyms_recycler;
    RelativeLayout summaryLayout;
    TextView tvHeader;
    RecyclerView summary_recycler;
    FloatingActionButton next;
    Runnable runnable;
    Handler handler;
    ProgressDialog progressDialog;
    int timer = 0;
    int status = 0;
    int page = 0;
    int currentPage = 0;
    RequestQueue requestQueue;
    SynonymsAdapter synonymsAdapter;
    SynonymsSummaryAdapter synonymsSummaryAdapter;
    ArrayList<Synonym> synonyms = new ArrayList<>();
    ArrayList<Crossword> crosswords = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager layoutManager;
    TextView selected_word;
    GifImageView word_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synonym);


        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.DKGRAY);
        }

        requestQueue = Volley.newRequestQueue(this);
        handler = new Handler();

        if(progressDialog == null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
        }

        srl = findViewById(R.id.srl);
        gameLayout = findViewById(R.id.gameLayout);
        game_options_menu = findViewById(R.id.game_options_menu);
        //countdown = findViewById(R.id.countdown);
        question = findViewById(R.id.question);
        rules = findViewById(R.id.rules);
        divider = findViewById(R.id.divider);
        synonyms_recycler = findViewById(R.id.synonyms_recycler);
        summaryLayout = findViewById(R.id.summaryLayout);
        tvHeader = findViewById(R.id.tvHeader);
        summary_recycler = findViewById(R.id.summary_recycler);
        next = findViewById(R.id.next);
        selected_word = findViewById(R.id.selected_word);
        word_status = findViewById(R.id.word_status);



        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (gameLayout.getVisibility() == View.VISIBLE){
                    reloadData();
                }else {
                    srl.setRefreshing(false);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame(false);
            }
        });

        synonymsAdapter = new SynonymsAdapter(synonyms,this);
        synonymsSummaryAdapter = new SynonymsSummaryAdapter(synonyms,this);


        layoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        summary_recycler.setLayoutManager(layoutManager);
        summary_recycler.setAdapter(synonymsSummaryAdapter);

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                synonyms.remove(viewHolder.getAdapterPosition());
                if(synonyms.size() <= 0){
                    startGame(false);
                }else {
                    summary_recycler.getAdapter().notifyItemRemoved(viewHolder.getAdapterPosition());
                }
            }
        };

        new ItemTouchHelper(simpleCallback).attachToRecyclerView(summary_recycler);

        linearLayoutManager = new GridLayoutManager(this,2);
        synonyms_recycler.setLayoutManager(linearLayoutManager);
        synonyms_recycler.setAdapter(synonymsAdapter);

        reloadData();

        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SynonymActivity.this);
                bottomSheetDialog.setContentView(R.layout.rules_layout);
                bottomSheetDialog.setCanceledOnTouchOutside(true);
                bottomSheetDialog.show();
            }
        });
    }

    public void reloadData(){

        String synonymDataURL = "https://crossword-ddd3a.firebaseio.com/crossword.json";

        showProgressDialog();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, synonymDataURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response != null) {

                                JSONArray jsonArray = response.getJSONArray("crossword");

                                crosswords.clear();
                                for (int j=0;j < jsonArray.length(); j++){
                                    JSONObject jsonObject = jsonArray.getJSONObject(j);

                                    String type = jsonObject.getString("type");
                                    int t = jsonObject.getInt("timer");

                                    JSONArray jsonArray1 = jsonObject.getJSONArray("synonyms");

                                    ArrayList<Synonym> newSynonyms = new ArrayList<>();
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray1.getJSONObject(i);

                                        String key1 = jsonObject1.getString("key1");
                                        String key2 = jsonObject1.getString("key2");
                                        String description = jsonObject1.getString("description");

                                        Synonym synonym1 = new Synonym();
                                        synonym1.setKey1(key1);
                                        synonym1.setKey2(key2);
                                        synonym1.setDescription(description);
                                        newSynonyms.add(synonym1);

                                        Synonym synonym2 = new Synonym();
                                        synonym2.setKey1(key2);
                                        synonym2.setKey2(key1);
                                        synonym2.setDescription(description);
                                        newSynonyms.add(synonym2);
                                    }

                                    Crossword crossword = new Crossword();
                                    crossword.setSynonyms(newSynonyms);
                                    crossword.setType(type);
                                    crossword.setTimer(t);
                                    crosswords.add(crossword);
                                }

                                page = 0;
                                startGame(false);

                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        srl.setRefreshing(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        srl.setRefreshing(false);
                        hideProgressDialog();
                        error.printStackTrace();
                    }
                });
        requestQueue.add(request);
    }

    public void startGame(boolean retry){

        if (page >= 4){
            Intent intent = new Intent(SynonymActivity.this, ScoreActivity.class);
            intent.putExtra("easy",synonymsAdapter.easyAccuracy);
            intent.putExtra("medium",synonymsAdapter.mediumAccuracy);
            intent.putExtra("hard",synonymsAdapter.hardAccuracy);
            intent.putExtra("genius",synonymsAdapter.geniusAccuracy);
            startActivity(intent);
            finish();
            //Toast.makeText(SynonymActivity.this, synonymsAdapter.easyAccuracy + "%", Toast.LENGTH_LONG).show();
        } else {
            handler.removeCallbacks(runnable);
            synonyms.clear();
            Crossword crossword;
            if(!retry){
                crossword = crosswords.get(page);
                currentPage = page;
                page = page + 1;
            } else {
                crossword = crosswords.get(currentPage);
                page = currentPage + 1;
            }
            synonyms.addAll(crossword.getSynonyms());
            final String type = crossword.getType();
            if(type.equals("easy")) {
                TextView level = (TextView) findViewById(R.id.level);
                level.setText(type);
                //SwipeRefreshLayout background = (SwipeRefreshLayout) findViewById(R.id.srl);
            }
            else if (type.equals("medium")) {
                TextView level = (TextView) findViewById(R.id.level);
                level.setText(type);
                //SwipeRefreshLayout background = (SwipeRefreshLayout) findViewById(R.id.srl);
            }
            else if(type.equals("hard")) {
                TextView level = (TextView) findViewById(R.id.level);
                level.setText(type);
                //SwipeRefreshLayout background = (SwipeRefreshLayout) findViewById(R.id.srl);
            }
            else if(type.equals("genius")) {
                TextView level = (TextView) findViewById(R.id.level);
                level.setText(type);
                //level.setTextColor(Color.parseColor("#FFFFFF"));
                //SwipeRefreshLayout background = (SwipeRefreshLayout) findViewById(R.id.srl);
            }
            //final ProgressBar progressBar = findViewById(R.id.pbTimer);
            timer = crossword.getTimer();
            status = 0;
            //progressBar.setMax(timer);
            //progressBar.setProgress(status);

            runnable = new Runnable() {
                @Override
                public void run() {
                    if (timer > status){
                        status = status + 1;
                        //progressBar.setProgress(status);
                        handler.postDelayed(this,1000);
                        TextView countdown = (TextView) findViewById(R.id.countdown);
                        countdown.setText(String.valueOf(timer-status));
                    } else{
                        if (gameLayout.getVisibility()==View.VISIBLE) {
                            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SynonymActivity.this);
                            bottomSheetDialog.setContentView(R.layout.retry_layout);
                            bottomSheetDialog.setCanceledOnTouchOutside(false);
                            TextView retry = bottomSheetDialog.findViewById(R.id.retry);
                            Button retry_positive_button = bottomSheetDialog.findViewById(R.id.retry_positive_button);
                            Button retry_negative_button = bottomSheetDialog.findViewById(R.id.retry_negative_button);
                            retry.setText("Do you want to Retry?");
                            retry_positive_button.setText("Retry");
                            retry_positive_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    bottomSheetDialog.dismiss();
                                    startGame(true);
                                }
                            });

                            retry_negative_button.setText("Cancel");
                            retry_negative_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    bottomSheetDialog.dismiss();
                                    Intent intent = new Intent(SynonymActivity.this, GameActivity.class);
                                    startActivity(intent);
                                    finish();
                                    //Toast.makeText(SynonymActivity.this, "Unabled to start Game", Toast.LENGTH_LONG).show();
                                }
                            });

                            if(!bottomSheetDialog.isShowing()){
                                bottomSheetDialog.show();
                            }
                            /*AlertDialog.Builder builder1 = new AlertDialog.Builder(SynonymActivity.this);
                            builder1.setMessage("DO YOU WANT TO RETRY?");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "Yes",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            startGame(true);
                                        }
                                    });

                            builder1.setIcon(android.R.drawable.ic_dialog_alert);

                            builder1.setNegativeButton(
                                    "No",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Toast.makeText(SynonymActivity.this, "Unabled to start Game", Toast.LENGTH_LONG).show();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();

                            if(!alert11.isShowing()) {
                                alert11.show();
                            }*/
                            handler.removeCallbacks(this);
                        }
                        else {
                            handler.removeCallbacks(this);
                        }
                    }
                }
            };

            handler.postDelayed(runnable, 1000);

            Collections.shuffle(synonyms);

            synonymsSummaryAdapter.notifyDataSetChanged();
            synonymsAdapter.notifyDataSetChanged();
            Preferences.removePreference(Constants.FIRST_ENTRY);
            Preferences.removePreference(Constants.FIRST_ENTRY_ID);
            gameLayout.setVisibility(View.VISIBLE);
            summaryLayout.setVisibility(View.GONE);
        }

    }

    public void showProgressDialog() {
        try {
            if(progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.setCancelable(true);
                progressDialog.show();
            }
        } catch (Exception ignored) {
        }
    }

    public void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (Exception ignored) {
        }
    }
}