package com.intern.synonymgame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.intern.synonymgame.R;

public class ScoreActivity extends AppCompatActivity {

    Button play_again;
    float easy,medium,hard,genius;
    TextView easy_score,medium_score,hard_score,genius_score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        Window window = this.getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.DKGRAY);
        }

        easy = getIntent().getFloatExtra("easy",0);
        medium = getIntent().getFloatExtra("medium",0);
        hard = getIntent().getFloatExtra("hard",0);
        genius = getIntent().getFloatExtra("genius",0);

        String easy_round = String.format("%.2f",easy);
        String medium_round = String.format("%.2f",medium);
        String hard_round = String.format("%.2f",hard);
        String genius_round = String.format("%.2f",genius);

        play_again = findViewById(R.id.play_again);
        easy_score = findViewById(R.id.easy_score);
        medium_score = findViewById(R.id.medium_score);
        hard_score = findViewById(R.id.hard_score);
        genius_score = findViewById(R.id.genius_score);

        easy_score.setText(easy_round + "%");
        medium_score.setText(medium_round + "%");
        hard_score.setText(hard_round + "%");
        genius_score.setText(genius_round + "%");

        play_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScoreActivity.this, SynonymActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}