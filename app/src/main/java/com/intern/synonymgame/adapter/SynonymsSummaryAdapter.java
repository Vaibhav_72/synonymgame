package com.intern.synonymgame.adapter;

import android.app.Activity;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.intern.synonymgame.R;
import com.intern.synonymgame.model.Synonym;

import java.util.ArrayList;
import java.util.Locale;

public class SynonymsSummaryAdapter extends RecyclerView.Adapter<SynonymsSummaryAdapter.ViewHolder> {
    private ArrayList<Synonym> synonyms;
    private Activity activity;
    TextToSpeech textToSpeech;

    public SynonymsSummaryAdapter(ArrayList<Synonym> synonyms, final Activity activity) {
        this.synonyms = synonyms;
        this.activity = activity;

        textToSpeech = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS){
                    int result = textToSpeech.setLanguage(Locale.ENGLISH);
                    if(result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                        Toast.makeText(activity, "Error!!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_answer_summary,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Synonym synonym = synonyms.get(position);
        holder.synonym_description.setVisibility(View.GONE);
        holder.synonym1.setText(synonym.getKey1());
        holder.synonym2.setText(synonym.getKey2());
        holder.synonym_description.setText(synonym.getDescription());
        holder.answers_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.synonym_description.getVisibility() == View.GONE){
                    holder.synonym_description.setVisibility(View.VISIBLE);
                    textToSpeech.speak(holder.synonym1.getText() + "or" + holder.synonym2.getText() + "means" + holder.synonym_description.getText(),TextToSpeech.QUEUE_FLUSH,null);
                }else if(holder.synonym_description.getVisibility() == View.VISIBLE){
                    holder.synonym_description.setVisibility(View.GONE);
                }
                //holder.synonym_description.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return synonyms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView answers_cardview;
        TextView synonym1;
        TextView synonym2;
        TextView synonym_description;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            answers_cardview = itemView.findViewById(R.id.answers_cardview);
            synonym1 = itemView.findViewById(R.id.synonym1);
            synonym2 = itemView.findViewById(R.id.synonym2);
            synonym_description = itemView.findViewById(R.id.synonym_description);
        }
    }
}
